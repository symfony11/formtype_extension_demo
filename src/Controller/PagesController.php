<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="app_home", requirements={'GET'})
     */
    public function home(Request $request)
    {
        return $this->render('landing/home.html.twig', [
            ''
        ]);
    }
}
