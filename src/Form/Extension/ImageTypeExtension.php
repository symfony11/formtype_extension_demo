<?php

namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ImageTypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes()
    {
        return [FileType::class];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // make it possible for FileType fields to have an 'image_property' option
        $resolver->setDefined(['image_property']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (isset($options['image_property'])) {
            // should map Media entity's data at this point
            $parentData = $form->getParent()->getData();

            $imageUrl = null;
            if (null != $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $imageUrl = $accessor->getValue($parentData, $options['image_property']);
            }

            // sets an image_url variable that will be accessible on the front-end after the form is rendered
            $view->vars['image_url'] = $imageUrl;
        }
    }
}